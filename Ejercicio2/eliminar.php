<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style>
    body{
      padding:10px;
      font: 13px/150% Arial, Helvetica, sans-serif;
      color: #040404;
    }
    a{
      text-decoration: none;
      color:blue;
    }
    a:hover{
    }
    table{
      margin:20px 0;
      width:50%;
    }
    th{
      text-align: center;
    }
    td, th{
      padding:5px;
      border-bottom:solid 1px #ccc;
      margin:0;
    }
    a.btn{
      border:solid 1px #c7c7c7;
      padding:3px 7px;
      text-decoration: none;
      color: #040404;
      font-size:12px;
      background: #e3e3e3;
    }
    a.btn:hover{
      background: #dbdbdb;
    }
    .btn-success{
      background: #00BF00 !important;
      border: solid 1px #00BF00 !important;
      color:#FFF !important;
    }
  </style>
</head>
<body>

  <?php

    require 'vendor/autoload.php';

    $yo = new MongoDB\Client;
    $bd = $yo->test_developer->usuarios_datos;

    $ID_usuario = $_POST['ID_usuario'];

    $bd->delete( ['_id' => $ID_usuario]);

    $cursor = $bd->find();

    echo "<table style = 'width: 100%'>
      <tr>

        <th width='400'; style='background:#CCC; color:#000'>
        ID_usuario
        </th>    
        <th width='300'; style='background:#CCC; color:#000'>
        Nombres
        </th>
        <th width='300'; style='background:#CCC; color:#000'>
        Apellidos
        </th>
        <th width='400'; style='background:#CCC; color:#000'>
        Ciudad
        </th>
        <th width='800'; style='background:#CCC; color:#000'>
        Correo
        </th>
        <th width='400'; style='background:#CCC; color:#000'>
        Telefono
        </th>
        <th width='300'; style='background:#CCC; color:#000'>
        Fecha de Nacimiento
        </th>
      </tr>
      </table>";

    // recorrer el resultado
    foreach ($cursor as $documento) {

      $ID_usuario = $documento['_id'];
      $nombres = $documento['Nombres'];
      $apellidos = $documento['Apellidos'];
      $ciudad = $documento['Ciudad'];
      $correo = $documento['Correo'];
      $telefono = $documento['Telefono'];
      $fecha = $documento['Fecha'];       

    echo "<table style = 'width:100%' >
      <tr>
        <td width='100'>
           $ID_usuario
          </td>
        <td width='600'>
         $nombres
        </td>
        <td width='600'>
         $apellidos
        </td>
        <td width='1000'>
         $ciudad
        </td>
        <td width='1400'>
         $correo
        </td>
        <td width='600'>
         $telefono
        </td>
        <td width='300'>
         $fecha
        </td>
       </tr> 
      </table>";
    }
  ?>
  <form action="edit.html" method="post" name="form2">
    <td>
      <a href=\"edit.php?id=$res[_id]\">
        <input type="submit" value="Modificar" />
      </a> |
      <a href=\"delete.html?id=$res[_id]\" onClick=\"return confirm('Are you sure you want to delete?')\">
        <input type="submit" value="Eliminar" />
      </a>
    </td>; 
  </form>
</body>
</html>a