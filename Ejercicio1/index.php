<!DOCTYPE html>
<html>
<head>
  <title>Exportar a Excel con PHP</title>
  <style>
    body{
      padding:10px;
      font: 13px/150% Arial, Helvetica, sans-serif;
      color: #040404;
    }
    a{
      text-decoration: none;
      color:blue;
    }
    a:hover{
    }
    table{
      margin:20px 0;
      width:50%;
    }
    th{
      text-align: center;
    }
    td, th{
      padding:5px;
      border-bottom:solid 1px #ccc;
      margin:0;
    }
    a.btn{
      border:solid 1px #c7c7c7;
      padding:3px 7px;
      text-decoration: none;
      color: #040404;
      font-size:12px;
      background: #e3e3e3;
    }
    a.btn:hover{
      background: #dbdbdb;
    }
    .btn-success{
      background: #00BF00 !important;
      border: solid 1px #00BF00 !important;
      color:#FFF !important;
    }
  </style>
</head>
<body>

  <?php

    require 'vendor/autoload.php';

    $con = new MongoDB\Client;
    $bd = $con->ejercicio1->erictel;

    $documento = $bd->insertOne( ['Nombres' => 'Marielb De Nazareth', 'Apellidos' => 'Marquez Ramirez' , 'email' => 'mmarielb@gmail.com' , 'edad' => '28'] );
    $documento = $bd->insertOne( ['Nombres' => 'Endys Jose', 'Apellidos' => 'Castro Marquez' , 'email' => 'ejcdrna@gmail.com', 'edad' => '30'] );
    $documento = $bd->insertOne( ['Nombres' => 'Gustavo Andres', 'Apellidos' => 'Marquez Ramirez' , 'email' => 'gama1269@gmail.com' , 'edad' => '12'] );
    $documento = $bd->insertOne( ['Nombres' => 'Gustavo Andres', 'Apellidos' => 'Marquez Ramirez' , 'email' => 'gama1269@gmail.com' , 'edad' => '40'] );
    $documento = $bd->insertOne( ['Nombres' => 'Mariela Rafaela', 'Apellidos' => 'Ramirez Urbina' , 'email' => 'marygus171@gmail.com' , 'edad' => '48'] );
    $documento = $bd->insertOne( ['Nombres' => 'Gustavo Adolfo', 'Apellidos' => 'Marquez Aranda' , 'email' => 'gama_1269@gmail.com' , 'edad' => '48'] );
    $documento = $bd->insertOne( ['Nombres' => 'Maryangel Carolina', 'Apellidos' => 'Araujo Marin' , 'email' => 'maryangelcarolina@gmail.com' , 'edad' => '29']);
    $documento = $bd->insertOne( ['Nombres' => 'Yassir', 'Apellidos' => 'Uzcategui Guitierrez' , 'email' => 'yassiruzg@gmail.com' , 'edad' => '31'] );
    $documento = $bd->insertOne( ['Nombres' => 'Maggie Andreina', 'Apellidos' => 'Castro Marquez' , 'email' => 'maggiecm@gmail.com' , 'edad' => '1'] );
    $documento = $bd->insertOne( ['Nombres' => 'Lucy Andreina', 'Apellidos' => 'Castro Marquez' , 'email' => 'lucycm@gmail.com' , 'edad' => '1']);

    $cursor = $bd->find();

    echo "<table>
      <tr>
        <th width='200'; style='background:#CCC; color:#000'>
        Nombres
        </th>
        <th width='200'; style='background:#CCC; color:#000'>
        Apellidos
        </th>
        <th width='200'; style='background:#CCC; color:#000'>
        Email
        </th>
        <th width='200'; style='background:#CCC; color:#000'>
        Edad
        </th>
      </tr>
      </table>";

    foreach ($cursor as $documento) {
        $nombres = $documento['Nombres'];
        $apellidos = $documento['Apellidos'];
        $email = $documento['email'];
        $edad = $documento['edad'];

    echo "<table>
      <tr>
        <td width='200';>
         $nombres
        </td>
        <td width='200';>
        $apellidos
        </td>
        <td width='200';>
        $email
        </td>
        <td width='200';>
        $edad
        </td>
      </tr>
      </table>";
    }
?>
  <a class="btn btn-success" href="export_excel.php">Exportar a Excel</a>
</body>
</html>